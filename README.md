# compl - docker-compose wrapper
compl is a simple wrapper for docker compose which allows you to start and stop compose stacks from anywhere

## Install
### Debian
#### Repo
Add my repo:
```
sudo echo "deb https://repo.ejected.space/debian/ stretch main" >> /etc/apt/sources.list
```
The GPG key:
```
wget -qO - https://repo.ejected.space/debian/pubkey.txt | sudo apt-key add -
```
And Install:
```
sudo apt-get update
sudo apt-get install compl
```

#### Manual
Download [The latest release](https://gitlab.com/ejectedspace/compl/-/jobs/artifacts/master/download?job=packaging%3Adebian%3Adev) extract it and install with:
```
sudo dpkg -i <compl.deb>
```
